import java.util.Random;

class MergeSort {
    /**
     * złożoność obliczeniowa: O(n*log n)
     * złożoność pamięciowa: Omega(n)
     */
    static int[] mergeSort(int[] array) {
        if (array.length == 2) {
            swapFirstTwoElementsIn(array);
        }
        //divide array into 2 arrays
        if (array.length >= 3) {
            int leftArrayLength = array.length / 2;
            int rightArrayLength = array.length - leftArrayLength;
            int[] leftArray = new int[leftArrayLength];
            int[] rightArray = new int[rightArrayLength];
            for (int i = 0; i < leftArrayLength; i++) {
                leftArray[i] = array[i];
            }
            for (int i = 0; i < rightArrayLength; i++) {
                rightArray[i] = array[i + leftArrayLength];
            }
            //recursion
            leftArray = mergeSort(leftArray);
            rightArray = mergeSort(rightArray);
            //merge two arrays
            array = mergeArrays(leftArray, rightArray);
        }
        return array;
    }

    /**
     * method sorts elements on indexes 0 and 1
     */
    private static void swapFirstTwoElementsIn(int[] array) {
            if (array[0] > array[1]) {
                int temp = array[0];
                array[0] = array[1];
                array[1] = temp;
            }
        }

    /**
     * method merges two arrays into 1, length of arrays does not matter
     */
    private static int[] mergeArrays(int[] left, int[] right) {
        int[] array = new int[left.length + right.length];
        int indexLeft = 0;
        int indexRight = 0;
        int indexMerged = 0;
        //case when both array have the same length
        while (indexLeft != left.length && indexRight != right.length) {
            if (left[indexLeft] <= right[indexRight]) {
                array[indexMerged] = left[indexLeft];
                indexLeft++;
                indexMerged++;
            } else {
                array[indexMerged] = right[indexRight];
                indexRight++;
                indexMerged++;
            }
        }
        //case when left array is used, and there are still some elements left in right array
        if (indexLeft == left.length) {
            while (indexRight != right.length) {
                array[indexMerged] = right[indexRight];
                indexRight++;
                indexMerged++;
            }
        } else {
            //case when right array is used, and there are still some elements left in left array
            while (indexLeft != left.length) {
                for (int i = 0; i < left.length - indexLeft + 1; i++) {
                    array[indexMerged] = left[indexLeft];
                    indexLeft++;
                    indexMerged++;
                }
            }
        }
        return array;
    }

    /**
     * Return array of random integers
     *
     * @param size   the number of values to generate
     * @param origin the origin (inclusive) of each random value
     * @param bound  the bound (exclusive) of each random value
     */
    static int[] createRandomArray(int size, int origin, int bound) {
        int[] array = new int[size];
        Random random = new Random();
        for (int i = 0; i < size; i++) {
            array[i] = random.ints(1, origin, bound).findFirst().getAsInt();
        }
        return array;
    }
}

