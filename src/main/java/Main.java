import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] array = MergeSort.createRandomArray(40, -10, 20);
        System.out.println("Array before sorting:");
        System.out.println(Arrays.toString(array));
        System.out.println("Sorted array:");
        System.out.println(Arrays.toString(MergeSort.mergeSort(array)));
    }
}
